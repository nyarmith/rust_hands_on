mod map;
mod map_builder;
mod player;
mod camera;

// this section exists to save us from constant thing::other::thing namespacing stuff, kind of like "using" in c++
// I think this is convention, aka there is nothing special about the name "prelude"; we are just
// defining a new (private) module for namespacing acces
mod prelude {
    pub use bracket_lib::prelude::*;    // re-export bracket-lib's prelude here
    pub const SCREEN_WIDTH: i32 = 80;
    pub const SCREEN_HEIGHT: i32 = 50;
    pub const DISPLAY_WIDTH: i32 = SCREEN_WIDTH / 2;
    pub const DISPLAY_HEIGHT: i32 = SCREEN_HEIGHT / 2;
    pub use crate::map::*;  // re-export map as a public module from this prelude
    pub use crate::player::*;
    pub use crate::map_builder::*;
    pub use crate::camera::*;
    // note that "crate"
}

// splash those names over our code
use prelude::*;

struct State {
    map: Map,
    player: Player,
    camera: Camera,
}

impl State {
    fn new() -> Self {
        let mut rng = RandomNumberGenerator::new();
        let map_builder = MapBuilder::new(&mut rng);
        Self {
            map: map_builder.map,
            player: Player::new(map_builder.player_start),
            camera: Camera::new(map_builder.player_start)
        }
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        for i in 0..=1 {
            ctx.set_active_console(i);
            ctx.cls();
        }
        self.player.update(ctx, &self.map, &mut self.camera);
        self.map.render(ctx, &self.camera);
        self.player.render(ctx, &self.camera);
    }
}

fn main() -> BError {
    let context = BTermBuilder::new()
        .with_title("test dungeon crawler")
        .with_fps_cap(30.0)
        .with_dimensions(DISPLAY_WIDTH, DISPLAY_HEIGHT)
        .with_tile_dimensions(32, 32)
        .with_resource_path("resources/")
        .with_font("dungeon_font.png", 32, 32)
        .with_simple_console(DISPLAY_WIDTH, DISPLAY_HEIGHT, "dungeon_font.png")
        .with_simple_console_no_bg(DISPLAY_WIDTH, DISPLAY_HEIGHT, "dungeon_font.png")
        .build()?;
    // creates a two-console layer - one for map and one for player, makes it a little bit
    // transparent. Viewport is limited with a cam
    
    main_loop(context, State::new())
}
