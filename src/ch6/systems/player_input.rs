use crate::prelude::*;

#[system]
#[write_component(Point)]  // requests writeable access to a component type
#[read_component(Player)] // requests read-only access to a component type
pub fn player_input(
    ecs: &mut SubWorld, // world with only the components we care about
    // these macros work with legion's resource handler
    commands: &mut CommandBuffer,
    #[resource] key: &Option<VirtualKeyCode>,
    #[resource] turn_state: &mut TurnState
) {
    if let Some(key) = key {
        let delta = match key {
            VirtualKeyCode::Left => Point::new(-1, 0),
            VirtualKeyCode::Right => Point::new(1, 0),
            VirtualKeyCode::Up => Point::new(0, -1),
            VirtualKeyCode::Down => Point::new(0, 1),
            _ => Point::new(0, 0),
        };

        if delta.x != 0 || delta.y != 0 {
            let mut players = <(Entity, &Point)>::query().filter(component::<Player>());
            players.iter(ecs).for_each(|(entity, pos)| {
                let dest = *pos + delta;
                commands.push(((), WantsToMove{entity: *entity, destination: dest}));
            });
            *turn_state = TurnState::PlayerTurn;
        }
    }
}
