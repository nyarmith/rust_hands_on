use std::io::stdin;

fn what_is_your_name() -> String {
    let mut your_name = String::new();

    stdin()
        .read_line(&mut your_name)
        .expect("Failed to read line!");

    your_name.trim().to_lowercase()
}

fn main() {
    println!("Hello, what's your name?");

    let your_name = what_is_your_name();

    let visitor_list = ["bert", "ernie", "elmo"];

    //for i in 0..visitor_list.len() {
    //    if visitor_list[i] == your_name {
    //        println!("Hello, {}", your_name);
    //    }
    //}
    
    let mut allow_them_in = false;
    for visitor in &visitor_list{
        if visitor == &your_name {  // I guess the & converts String to a &str
            allow_them_in = true;
        }
    }

    if allow_them_in {
        println!("Welcome, {}", your_name);
    } else {
        println!("Sorry, this is an invite-only event");
    }
}
