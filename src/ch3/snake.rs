use bracket_lib::prelude::*;
use std::ops::{Add, Mul, Sub};
use std::collections::VecDeque;

enum GameMode {
    Menu,
    Playing,
    End,
}

#[derive(Copy, Clone)] // no idea how these work concretely
struct Vec2i {
    x : i32,
    y: i32
}

impl Vec2i {
    fn new(x : i32, y : i32) -> Vec2i {
        Self { x, y }
    }
}

impl Add<Vec2i> for Vec2i {
    type Output = Vec2i;
    fn add(self, other: Self) -> Vec2i {
        Vec2i::new(self.x + other.x, self.y + other.y)
    }
}

impl Sub<Vec2i> for Vec2i {
    type Output = Vec2i;
    fn sub(self, other: Self) -> Vec2i {
        Vec2i::new(self.x - other.x, self.y - other.y)
    }
}

impl Mul<i32> for Vec2i {
    type Output = Vec2i;
    fn mul(self, scalar: i32) -> Vec2i {
        Vec2i::new(self.x*scalar, self.y*scalar)
    }
}

impl PartialEq for Vec2i {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

struct Player {
    pos : Vec2i,
    vel : Vec2i,
    len : usize,
    tail : VecDeque<Vec2i>
}

impl Player {
    fn new(x: i32, y: i32) -> Self {
        Player {
            pos : Vec2i::new(x,y),
            vel : Vec2i::new(1,0), //start the player going right
            len : 2, // start the player with length 2
            tail : VecDeque::new()
        }
    }

    fn render(&mut self, ctx: &mut BTerm) {
        ctx.set(
            self.pos.x,
            self.pos.y,
            YELLOW,
            BLACK,
            to_cp437('@'));

        for p in self.tail.iter() {
            ctx.set(
                p.x,
                p.y,
                YELLOW,
                BLACK,
                to_cp437('#'));
        }
            
    }

    fn update(&mut self) {
        //old pos is now tail
        self.tail.push_front(self.pos);
        // update head pos
        self.pos = self.pos + self.vel;

        if self.tail.len() > self.len {
            self.tail.pop_back();
        }
    }

    fn eat(&mut self) {
        self.len += 1;
    }

    fn is_self_collide(&mut self) -> bool {
        self.tail.contains(&self.pos)
    }
}

struct Treat {
    pos : Vec2i
}

impl Treat {
    fn new(min_x : i32, max_x : i32, min_y : i32, max_y : i32) -> Treat {
        let mut random = RandomNumberGenerator::new();
        Treat {
            pos : Vec2i::new(random.range(min_x, max_x),random.range(min_y, max_y))
        }
    }

    fn render(&mut self, ctx: &mut BTerm) {
        ctx.set(
            self.pos.x,
            self.pos.y,
            GREEN,
            BLACK,
            to_cp437('*'),
            );
    }

    fn hit_obstacle(&self, player : &Player) -> bool {
        player.pos.x == self.pos.x && player.pos.y == self.pos.y
    }
}


struct BBox {
    top_left : Vec2i,
    bot_right : Vec2i
}

impl BBox {
    fn new(tl_x: i32, tl_y : i32, bl_x : i32, bl_y : i32) -> Self {
        BBox {
            top_left  : Vec2i::new(tl_x,tl_y),
            bot_right : Vec2i::new(bl_x,bl_y)
        }
    }

    fn render(&mut self, ctx: &mut BTerm) {
        // herozontal bars
        for x in self.top_left.x..self.bot_right.x {
            ctx.set(
                x,
                self.top_left.y,
                RED,
                BLACK,
                to_cp437('*'),
                );

            ctx.set(
                x,
                self.bot_right.y,
                RED,
                BLACK,
                to_cp437('*'),
                );
        }

        // vertical bars
        for y in self.top_left.y..self.bot_right.y{
            ctx.set(
                self.top_left.x,
                y,
                RED,
                BLACK,
                to_cp437('*'),
                );

            ctx.set(
                self.bot_right.x,
                y,
                RED,
                BLACK,
                to_cp437('*'),
                );
        }
    }

    fn hit_obstacle(&self, player : &Player) -> bool {
        player.pos.x == self.top_left.x || player.pos.x == self.bot_right.x 
            || player.pos.y == self.top_left.y || player.pos.y == self.bot_right.y
    }
}

struct State {
    player : Player,
    frame_time: f32,
    obstacle: BBox,
    treat: Treat,
    mode: GameMode,
    score: i32,
}

const SCREEN_WIDTH : i32 = 80;
const SCREEN_HEIGHT : i32 = 50;
const FRAME_DURATION : f32 = 75.0;
impl State {
    fn new() -> Self {
        State {
            player: Player::new(5,25),
            frame_time: 0.0,
            obstacle: BBox::new(0,0,SCREEN_WIDTH-1,SCREEN_HEIGHT-1),
            treat : Treat::new(1,SCREEN_WIDTH-2,1,SCREEN_HEIGHT-2),
            mode : GameMode::Menu,
            score: 0,
        }
    }

    fn main_menu(&mut self, ctx: &mut BTerm) {
        ctx.cls();
        ctx.print_centered(5, "Welcome to Snakey Boi");
        ctx.print_centered(8, "(P) Play Game");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true,
                _ => {}
            }
        }
    }

    fn restart(&mut self) {
        self.player = Player::new(5,25);
        self.frame_time = 0.0;
        self.mode = GameMode::Playing;
    }

    fn play(&mut self, ctx: &mut BTerm) {
        ctx.cls_bg(NAVY);
        self.frame_time += ctx.frame_time_ms;
        if self.frame_time > FRAME_DURATION {
            self.frame_time = 0.0;
            self.player.update(); // normal game logic operation
        }

        match ctx.key {
            Some(VirtualKeyCode::Up) => self.player.vel    = Vec2i::new(0,-1),
            Some(VirtualKeyCode::Left) => self.player.vel  = Vec2i::new(-1,0),
            Some(VirtualKeyCode::Right) => self.player.vel = Vec2i::new(1,0),
            Some(VirtualKeyCode::Down) => self.player.vel  = Vec2i::new(0,1),
            _ => {},
        }

        self.treat.render(ctx);
        self.player.render(ctx);
        self.obstacle.render(ctx);

        ctx.print(0,0,"Press arrow keys to turn.");
        ctx.print(0,1,&format!("Score: {}", self.score));

        if self.obstacle.hit_obstacle(&self.player) || self.player.is_self_collide() {
            self.mode = GameMode::End;
        }

        if self.treat.hit_obstacle(&self.player){
            self.player.eat();
            self.score += 5;
            self.treat = Treat::new(1,SCREEN_WIDTH-2,1,SCREEN_HEIGHT-2);
        }

    }

    fn dead(&mut self, ctx: &mut BTerm) {
        ctx.cls();
        ctx.print_centered(5, "You are dead!");
        ctx.print_centered(6, "You earned a few points");
    }
}

// I guess here the "trait" is GameState and the struct getting a trait is State
impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        match self.mode {
            GameMode::Menu => self.main_menu(ctx),
            GameMode::End => self.dead(ctx),
            GameMode::Playing => self.play(ctx),
        }
    }
}

// default error type of Bracket-lib is BError

fn main() -> BError {
    let context = BTermBuilder::simple80x50()
        .with_title("Flappy Dragon") // see, we chain parameters in the method to build
        .build()?;

    // so, this is how our terminal library works, very simple
    main_loop(context, State::new())
}
