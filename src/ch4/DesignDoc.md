Name:
Wizard Run

Short Description:
A dungeon crawler (that I'd like to port to iOS) that uses four directional movements(swiped) that either attack or open an interaction menu. We also features an autosearch button to basically stay hands-off until interesting things happen.

Each level offers increased difficulty and more whacky magical traps and sequences to trigger. Watch for flying books!

Story
The goal is to get to the top of the wizard tower to steal the time tome(or other) and escape. The ancient time tome is said to hold the secrets of the gods, immortality and the origin of the world itself! However, many have come to try learning its secrets only to become history. Be warned, all ye who hail here!

Basic Game Loops
1. Enter dungon level
2. Explore map for next level
3. Encounter enemies to fight or run away
4. Find power-ups and keys to the magic elements
5. Locate the time tome, and then get out alive!


Minimum Viable Product
1. Create basic dungeon map
2. Let player walk around
3. Spawn monsters to interact with
4. Add combat system
5. Add potions that heal or temporarily mess up directionality
6. Display game over screen with a score when the player dies
7. Add the time tome to the level and the player wins after they get it and walk back out of the entrance

Stretch Goals
1. Add Field of View
2. Add more interesting dungeon designs
3. Add dungeon themes
4. Add multiple layers to the dungeon, with the time tome being the last one
5. Add different weapons to the game
6. Use data-driven design for spawning enemies and their stats
7. Make combat visceral with visual effects
8. Score
9. Projectile weapons
10. Magic buffs and debuggs (that fade with time or a condition)

misc notes/links:
https://mozilla.github.io/firefox-browser-architecture/experiments/2017-09-06-rust-on-ios.html
https://dev.to/wadecodez/exploring-rust-for-native-ios-game-development-2bna
https://itnext.io/rust-native-ios-touch-events-8b01418e0f3b
