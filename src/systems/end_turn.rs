use crate::prelude::*;

#[system]
#[read_component(Health)]
#[read_component(Point)]
#[read_component(Player)]
#[read_component(MacGuffin)]
pub fn end_turn(
    ecs: &SubWorld,
    #[resource] turn_state: &mut TurnState,
    #[resource] map: &Map
) {
    let mut player_state = <(&Health,&Point)>::query().filter(component::<Player>());
    let mut macguffin = <&Point>::query().filter(component::<MacGuffin>());
    let macguffin_default = Point::new(-1, -1);
    let macguffin_pos = macguffin.iter(ecs).nth(0).unwrap_or(&macguffin_default);

    let current_state = turn_state.clone();
    let mut new_state = match current_state {
        TurnState::AwaitingInput => return,
        TurnState::PlayerTurn => TurnState::MonsterTurn,
        TurnState::MonsterTurn => TurnState::AwaitingInput,
        _ => current_state,
    };

    // handle special case of player's health going to 0, also of victory while we're here
    player_state.iter(ecs).for_each(|(hp,pos)| {
        if hp.current < 1 {
            new_state = TurnState::GameOver;
        }

        if pos == macguffin_pos {
            new_state = TurnState::Victory;
        }

        let idx = map.point2d_to_index(*pos);
        if map.tiles[idx] == TileType::Exit {
            new_state = TurnState::NextLevel;
        }
    });

    *turn_state = new_state;
}
