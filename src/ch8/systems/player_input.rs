use crate::prelude::*;

#[system]
#[read_component(Point)] // requests writeable access to a component type
#[read_component(Player)]
#[read_component(Enemy)]
#[write_component(Health)] // requests read-write access to a component type
pub fn player_input(
    ecs: &mut SubWorld, // world with only the components we care about
    // these macros work with legion's resource handler
    commands: &mut CommandBuffer,
    #[resource] key: &Option<VirtualKeyCode>,
    #[resource] turn_state: &mut TurnState,
) {
    if let Some(key) = key {
        let delta = match key {
            VirtualKeyCode::Left => Point::new(-1, 0),
            VirtualKeyCode::Right => Point::new(1, 0),
            VirtualKeyCode::Up => Point::new(0, -1),
            VirtualKeyCode::Down => Point::new(0, 1),
            _ => Point::new(0, 0),
        };

        let mut still = false;
        if delta.x == 0 && delta.y == 0 {
            still = true;
        }

        let mut players = <(Entity, &Point)>::query().filter(component::<Player>());
        let (player_entity, destination) = players.iter(ecs)
            .find_map(|(entity,pos)| Some((*entity, *pos + delta)) )
            .unwrap();

        let mut enemies = <(Entity, &Point)>::query().filter(component::<Enemy>());
        let mut hit_something = false;

        enemies.iter(ecs)
            .filter(|(_,pos)| { **pos == destination })
            .for_each(|(entity, _)| {
                hit_something = true;
                still = false;
                commands.push(((),WantsToAttack{attacker: player_entity, victim: *entity}));
            });

        if !hit_something{
            if !still {
                println!("Move case");
                commands.push((
                    (),
                    WantsToMove {
                        entity: player_entity,
                        destination
                    }));
            } else {
                // we are idle and still, so we heal one
                if let Ok(mut health) = ecs.entry_mut(player_entity).unwrap().get_component_mut::<Health>() {
                    health.current = i32::min(health.max, health.current + 1);
                }
            }
            
        }
        *turn_state = TurnState::PlayerTurn;
    }
}
